package main

type Config struct {
	ApiId          int    `yaml:"api_id"`
	ApiHash        string `yaml:"api_hash"`
	BotToken       string `yaml:"bot_token"`
	AllowedUserIds []int  `yaml:"allowed_user_ids"`
}
